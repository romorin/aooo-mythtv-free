#ifndef SUBJECT_IMPL_H
#define SUBJECT_IMPL_H

#include "Subject.h"

#include <list>

template <class EVENT>
class SubjectImpl : public Subject<EVENT> {
public:
	SubjectImpl() {}
	virtual ~SubjectImpl() {}

	virtual void attach(Observer<EVENT>& observer)
	{
		if (std::find(_observers.begin(), _observers.end(), &observer) ==
			_observers.end())
		{
			_observers.push_back(&observer);
		}
	}

	virtual void notify(const EVENT& event)
	{
		for ( auto it = _observers.begin(); it != _observers.end(); ++it )
		{
			(*it)->update(event);
		}
	}

private:
	std::list<Observer<EVENT>*> _observers;
};

#endif
