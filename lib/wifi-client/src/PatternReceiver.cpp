#include "PatternReceiver.h"

#include <cstring>

PatternReceiver::PatternReceiver(const char* const pattern) :
	_pattern(pattern), _patternLength(strlen(pattern)), _index(0)
{
}

PatternReceiver::~PatternReceiver()
{
}

bool PatternReceiver::receivedChar(const char data)
{
	if (data == _pattern[_index])
	{
		++_index;
		if (_index == _patternLength)
		{
			_index = 0;
			return true;
		}
	}
	else
	{
		_index = 0;
	}
	return false;
}

bool PatternReceiver::onConnectionEnd()
{
	return false;
}

unsigned int PatternReceiver::patternSize()
{
	return _patternLength;
}
