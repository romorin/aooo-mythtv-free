#include "FilteringCharReceiver.h"

FilteringCharReceiver::FilteringCharReceiver(
	ResultHandler& onResult,
	PatternReceiver& prePattern, PatternReceiver& postPattern,
	BufferingReceiver& buffer) :
	_onResult(onResult), _prePattern(prePattern), _postPattern(postPattern),
	_buffer(buffer), _ongoingMatch(false)
{}

FilteringCharReceiver::~FilteringCharReceiver()
{}

bool FilteringCharReceiver::receivedChar(const char data)
{
	if (_ongoingMatch)
	{
		_buffer.receivedChar(data);

		if (_postPattern.receivedChar(data))
		{
			_buffer.trimEnd(_postPattern.patternSize());
			_onResult.onResult(_buffer.getBuffer());
			_ongoingMatch = false;
		}
	}
	else
	{
		if (_prePattern.receivedChar(data))
		{
			_ongoingMatch = true;
			_buffer.reset();
		}
	}
	return false;
}

bool FilteringCharReceiver::onConnectionEnd()
{
	return false;
}
