#include "DebugPrint.h"
#include "CharReceiver.h"
#include "HttpClientShimImpl.h"

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

const char* const HttpClientShimImpl::REQUEST_PRE_URI =
	"GET ";
const char* const HttpClientShimImpl::REQUEST_POST_URI =
	" HTTP/1.1\r\nHost: ";
const char* const HttpClientShimImpl::REQUEST_POST_HOST =
	"\r\nConnection: close\r\n\r\n";

HttpClientShimImpl::HttpClientShimImpl(
	const char* const ssid, const char* const password,
	Subject<LoopEvent>& loopSubject) :
	_currentRequest(nullptr), _requestString()
{
	DEBUG_PRINTF("Connecting to %s ", ssid);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		DEBUG_PRINT(".");
	}
	DEBUG_PRINTLN(" connected");

	loopSubject.attach(*this);
}

HttpClientShimImpl::~HttpClientShimImpl()
{
	WiFi.disconnect();
	DEBUG_PRINTLN("Disconnected");
}

HttpClientShim::GetReturnCode HttpClientShimImpl::get(
	CharReceiver& receiver,
	const char* const host, unsigned int port, const char* const uri)
{
	if (WiFi.status() != WL_CONNECTED) {
		DEBUG_PRINTLN("Wifi not connected!");
		return GetReturnCode::WIFI_NOT_CONNECTED;
	}
	if (strlen(host) >= MAX_HOST_SIZE)
	{
		DEBUG_PRINTLN("Host too long!");
		return GetReturnCode::HOST_TOO_LONG;
	}
	if (strlen(uri) >= MAX_URI_SIZE)
	{
		DEBUG_PRINTLN("Uri too long!");
		return GetReturnCode::URI_TOO_LONG;
	}
	if (_currentRequest != nullptr || ongoingRequest())
	{
		DEBUG_PRINTLN("connection already in use!]");
		return GetReturnCode::IN_USE;
	}

	DEBUG_PRINTF("\n[Connecting to %s ... ", host);
	if (_client.connect(host, port))
	{
		DEBUG_PRINTLN("connected]");
		_currentRequest = &receiver;

		DEBUG_PRINTLN("[Sending a request]");

		strcpy(_requestString, REQUEST_PRE_URI);
		strcat(_requestString, uri);
		strcat(_requestString, REQUEST_POST_URI);
		strcat(_requestString, host);
		strcat(_requestString, REQUEST_POST_HOST);

		_client.print(_requestString);
		return GetReturnCode::OK;
	}
	else
	{
		DEBUG_PRINTLN("connection failed!]");
		_client.stop();
		return GetReturnCode::CANNOT_CONNECT;
	}

	return GetReturnCode::ERROR;
}

void HttpClientShimImpl::update(const LoopEvent& event)
{
	if (_currentRequest != nullptr)
	{
		if (ongoingRequest())
		{
			while (_client.available())
			{
				_currentRequest->receivedChar(_client.read());
			}
		}
		else
		{
			_currentRequest->onConnectionEnd();
			_currentRequest = nullptr;

			_client.stop();
			DEBUG_PRINTLN("\n[Disconnected]");
		}
	}
}

bool HttpClientShimImpl::ongoingRequest()
{
	return _client.connected() || _client.available();
}
