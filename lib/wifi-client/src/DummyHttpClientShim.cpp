#include "DummyHttpClientShim.h"

#include <cstring>
#include "Arduino.h"

DummyHttpClientShim::DummyHttpClientShim(const char* const templ) :
	_template(templ) {}

DummyHttpClientShim::~DummyHttpClientShim() {}

HttpClientShim::GetReturnCode DummyHttpClientShim::get(CharReceiver& receiver,
	const char* const host, unsigned int port, const char* const uri)
{
	char result[strlen(_template)+10];
	sprintf(result, _template, millis());

	for (size_t i = 0; i < strlen(result); i++)
	{
		receiver.receivedChar(result[i]);
	}
	receiver.onConnectionEnd();

	return OK;
}
