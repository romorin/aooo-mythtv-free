#ifndef CHAR_RECEIVER_H
#define CHAR_RECEIVER_H

class CharReceiver
{
public:
	virtual ~CharReceiver() {}
	virtual bool receivedChar(const char data) = 0;
	virtual bool onConnectionEnd() = 0;
};

#endif
