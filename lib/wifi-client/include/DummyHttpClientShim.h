#ifndef DUMMY_HTTP_CLIENT_SHIM_H
#define DUMMY_HTTP_CLIENT_SHIM_H

#include "CharReceiver.h"
#include "HttpClientShim.h"

class DummyHttpClientShim : public HttpClientShim
{
public:
	DummyHttpClientShim(const char* const templ);
	virtual ~DummyHttpClientShim();

	virtual GetReturnCode get(CharReceiver& receiver,
		const char* const host, unsigned int port, const char* const uri);

private:
	const char* const _template;
};

#endif
