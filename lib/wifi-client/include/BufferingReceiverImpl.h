#ifndef BUFFERING_RECEIVER_IMPL_H
#define BUFFERING_RECEIVER_IMPL_H

#include "BufferingReceiver.h"

template <int T>
class BufferingReceiverImpl : public BufferingReceiver
{
public:
	BufferingReceiverImpl() :
		_index(0)
	{
		_buffer[T-1] = '\0';
	}

	virtual ~BufferingReceiverImpl() {}

	virtual bool receivedChar(const char data)
	{
		if (_index < T - 1)
		{
			_buffer[_index] = data;
		}
		++_index;

		return false;
	}

	virtual bool onConnectionEnd()
	{
		return false;
	}

	virtual bool overfilled(const unsigned int endTrim = 0)
	{
		return (_index - endTrim > T);
	}

	virtual void trimEnd(const unsigned int endTrim)
	{
		if (_index - endTrim < T - 1)
		{
			_buffer[_index - endTrim] = '\0';
		}
	}

	virtual const char* getBuffer()
	{
		return _buffer;
	}

	virtual void reset()
	{
		_index = 0;
	}

private:
	char _buffer[T];
	unsigned int _index;
};

#endif
