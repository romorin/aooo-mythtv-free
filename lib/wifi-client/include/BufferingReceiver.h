#ifndef BUFFERING_RECEIVER_H
#define BUFFERING_RECEIVER_H

#include "CharReceiver.h"

class BufferingReceiver : public CharReceiver
{
public:
	virtual ~BufferingReceiver() {}

	virtual bool overfilled(const unsigned int endTrim = 0) = 0;
	virtual void trimEnd(const unsigned int endTrim) = 0;
	virtual const char* getBuffer() = 0;
	virtual void reset() = 0;
};

#endif
