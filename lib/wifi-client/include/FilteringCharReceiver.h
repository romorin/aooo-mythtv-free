#ifndef FILTERING_CHAR_RECEIVER_H
#define FILTERING_CHAR_RECEIVER_H

#include "BufferingReceiver.h"
#include "CharReceiver.h"
#include "PatternReceiver.h"
#include "ResultHandler.h"

class FilteringCharReceiver : public CharReceiver
{
public:
	FilteringCharReceiver(ResultHandler& onResult,
		PatternReceiver& prePattern, PatternReceiver& postPattern,
		BufferingReceiver& buffer);
	virtual ~FilteringCharReceiver();

	virtual bool receivedChar(const char data);
	virtual bool onConnectionEnd();

private:
	ResultHandler& _onResult;
	PatternReceiver& _prePattern;
	PatternReceiver& _postPattern;
	BufferingReceiver& _buffer;

	bool _ongoingMatch;
};

#endif
