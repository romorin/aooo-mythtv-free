#ifndef RESULT_HANDLER_H
#define RESULT_HANDLER_H

class ResultHandler
{
public:
	virtual ~ResultHandler() {}
	virtual void onResult(const char* result) = 0;
};

#endif
