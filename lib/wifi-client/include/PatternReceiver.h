#ifndef PATTERN_RECEIVER_H
#define PATTERN_RECEIVER_H

#include "CharReceiver.h"

class PatternReceiver : public CharReceiver
{
public:
	PatternReceiver(const char* const pattern);
	virtual ~PatternReceiver();

	virtual bool receivedChar(const char data);
	virtual bool onConnectionEnd();
	virtual unsigned int patternSize();

private:
	const char* const _pattern;
	const unsigned int _patternLength;
	unsigned int _index;
};

#endif
