#ifndef HTTP_CLIENT_SHIM_IMPL_H
#define HTTP_CLIENT_SHIM_IMPL_H

#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#include "include/Subject.h"
#include "LoopEvent.h"
#include "include/Observer.h"

#include "CharReceiver.h"
#include "HttpClientShim.h"

#ifndef MAX_HOST_SIZE
#define MAX_HOST_SIZE 100
#endif

#ifndef MAX_URI_SIZE
#define MAX_URI_SIZE 100
#endif

#define GET_STRING_SIZE 50 + MAX_HOST_SIZE + MAX_URI_SIZE

class HttpClientShimImpl : public HttpClientShim, public Observer<LoopEvent>
{
public:
	HttpClientShimImpl(const char* const ssid, const char* const password,
		Subject<LoopEvent>& loopSubject);
	virtual ~HttpClientShimImpl();

	virtual GetReturnCode get(CharReceiver& receiver,
		const char* const host, unsigned int port, const char* const uri);

	virtual void update(const LoopEvent& event);

private:
	bool ongoingRequest();

	WiFiClient _client;
	CharReceiver* _currentRequest;
	char _requestString[GET_STRING_SIZE];

	static const char* const REQUEST_PRE_URI;
	static const char* const REQUEST_POST_URI;
	static const char* const REQUEST_POST_HOST;
};

#endif
