#ifndef HTTP_CLIENT_SHIM_H
#define HTTP_CLIENT_SHIM_H

#include "CharReceiver.h"

class HttpClientShim
{
public:
	enum GetReturnCode { OK, WIFI_NOT_CONNECTED, HOST_TOO_LONG, URI_TOO_LONG,
		CANNOT_CONNECT, IN_USE, ERROR };

	virtual ~HttpClientShim() {}

	virtual GetReturnCode get(CharReceiver& receiver,
		const char* const host, unsigned int port, const char* const uri) = 0;
};

#endif
