#include "ArduinoInterruptTable.h"

#define INTERRUPT_ARRAY_SIZE 10

static unsigned char _size = 0;
static unsigned char _numbers[INTERRUPT_ARRAY_SIZE];
static InterruptHandler* volatile _handlers[INTERRUPT_ARRAY_SIZE];

ArduinoInterruptTable::ArduinoInterruptTable() {}

ArduinoInterruptTable::~ArduinoInterruptTable() {}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_0(void)
{
	_handlers[0]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_1(void)
{
	_handlers[1]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_2(void)
{
	_handlers[2]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_3(void)
{
	_handlers[3]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_4(void)
{
	_handlers[4]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_5(void)
{
	_handlers[5]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_6(void)
{
	_handlers[6]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_7(void)
{
	_handlers[7]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_8(void)
{
	_handlers[8]->ping();
}

void ICACHE_RAM_ATTR ArduinoInterruptTable::Interrupt_9(void)
{
	_handlers[9]->ping();
}

void ArduinoInterruptTable::add(const unsigned char number,
	InterruptHandler& handler, const unsigned char  mode)
{
	byte i = 0;
	for (; i < _size; ++i)
	{
		if (_numbers[i] == number)
		{
			break;
		}
	}
	if (i == _size)
	{
		if (_size == INTERRUPT_ARRAY_SIZE)
		{
			return;
		}
		_size++;
	}
	_numbers[i] = number;
	_handlers[i] = &handler;

	switch (i)
	{
		case 0:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_0, mode);
			break;
		case 1:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_1, mode);
			break;
		case 2:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_2, mode);
			break;
		case 3:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_3, mode);
			break;
		case 4:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_4, mode);
			break;
		case 5:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_5, mode);
			break;
		case 6:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_6, mode);
			break;
		case 7:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_7, mode);
			break;
		case 8:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_8, mode);
			break;
		case 9:
			attachInterrupt(number, ArduinoInterruptTable::Interrupt_9, mode);
			break;
	}
}
