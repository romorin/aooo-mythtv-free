#include "ShiftedCounter.h"

ShiftedCounter::ShiftedCounter(Pins<char>& serialDataPin,
	Pins<char>& inputClockPin, Pins<char>& outputClockPin, Timer& timer,
	size_t maxCount, const unsigned long hiPeriod) :
		_serialDataPin(serialDataPin), _inputClockPin(inputClockPin),
		_outputClockPin(outputClockPin), _timer(timer),
		_maxCount(maxCount), _hiPeriod(hiPeriod), _count(0), _ready(false),
		_clockFct([this]() { this->clock(); }),
		_unclockFct([this]() { this->unClock(); })
{
	_serialDataPin.write(0);
	clock();
}

ShiftedCounter::~ShiftedCounter()
{

}

bool ShiftedCounter::ready() const
{
	return _ready;
}

const size_t ShiftedCounter::currentCount() const
{
	return _count;
}

// delay between _inputClockPin and_outputClockPin necessary?

void ShiftedCounter::increment()
{
	if (_ready)
	{
		clock();
	}
}

size_t ShiftedCounter::maxCount() const
{
	return _maxCount;
}

void ShiftedCounter::clock()
{
	if (_count + 1 >= _maxCount)
	{
		_count = 0;
	}
	else
	{
		++_count;
	}
	_inputClockPin.write(0);
	_outputClockPin.write(1);
	_serialDataPin.write(_count + 1 >= _maxCount);

	_timer.once_us(_hiPeriod, _unclockFct);
}

void ShiftedCounter::unClock()
{
	_inputClockPin.write(1);
	_outputClockPin.write(0);

	if (_count == 0) {
		_ready = true;
	}

	if(!_ready)
	{
		_timer.once_us(_hiPeriod, _clockFct);
	}
}
