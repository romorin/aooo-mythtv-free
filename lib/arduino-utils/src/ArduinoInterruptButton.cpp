#include "ArduinoInterruptButton.h"

#include "Arduino.h"

ArduinoInterruptButton::ArduinoInterruptButton(
	const unsigned char pin,
	Observer<bool>& observer,
	InterruptTable& table,
	Subject<LoopEvent>& subject,
	unsigned long bounceTime
) : _pin(pin), _observer(observer), _bounceTime(bounceTime)
{
	pinMode(_pin, INPUT_PULLUP);
	table.add(digitalPinToInterrupt(_pin), *this, CHANGE);
	_buttonState = digitalRead(_pin);
	subject.attach(*this);
}

ArduinoInterruptButton::~ArduinoInterruptButton() {}

void ArduinoInterruptButton::ping()
{
	_bounceEvent = true;
}

void ArduinoInterruptButton::update(const LoopEvent& event)
{
	if (_bounceEvent)
	{
		_bounceEvent = false;
		_debouncing = true;
		_lastDebounceTime = event.millis;
	}
	if (_debouncing && (event.millis - _lastDebounceTime) > _bounceTime)
	{
		_debouncing = false;
		unsigned char reading = digitalRead(_pin);

		if (reading != _buttonState)
		{
			_buttonState = reading;
			_observer.update(_buttonState == LOW);
		}
	}
}
