#ifndef TIMER_IMPL_H
#define TIMER_IMPL_H

#include "include/Observer.h"
#include "include/Subject.h"

#include "LoopEvent.h"
#include "Timer.h"

#include <functional>

class TimerImpl : public Timer, public Observer<LoopEvent>
{
public:
	TimerImpl(Subject<LoopEvent>& subject);

	virtual ~TimerImpl();

	virtual bool attach_ms(const unsigned long milliseconds, std::function<void ()> callback);
	virtual bool attach_us(const unsigned long microseconds, std::function<void ()> callback);

	virtual bool once_ms(const unsigned long milliseconds, std::function<void ()> callback);
	virtual bool once_us(const unsigned long microseconds, std::function<void ()> callback);

	virtual void update(const LoopEvent& event);

private:
	LoopEvent _lastEvent;
	bool _once;
	bool _ms;
	unsigned long _period;
	unsigned long _lastTick;
	std::function<void ()> _callback;

	bool fill(bool once, bool ms, unsigned long time,
		std::function<void ()> callback);

	void reset();
	bool isLoaded();
};

#endif
