#ifndef SHIFTED_COUNTER_BUILDER_H
#define SHIFTED_COUNTER_BUILDER_H

#include "include/SubjectImpl.h"

#include "PinsDefinition.h"
#include "ArduinoPins.h"
#include "ShiftedCounter.h"
#include "LoopEvent.h"
#include "TimerImpl.h"

template <size_t MAX, unsigned long PERIOD_US>
class ShiftedCounterBuilder
{
public:
	ShiftedCounterBuilder(
		SubjectImpl<LoopEvent>& loopSubject,
		PinsDefinition<1> serialDataPinId,
		PinsDefinition<1> inputClockPinId,
		PinsDefinition<1> outputClockPinId
	) :
		_serialDataPin_id(serialDataPinId),
		_inputClockPin_id(inputClockPinId),
		_outputClockPin_id(outputClockPinId),
		_serialDataPin(_serialDataPin_id.id, _serialDataPin_id.activeHigh),
		_inputClockPin(_inputClockPin_id.id, _inputClockPin_id.activeHigh),
		_outputClockPin(_outputClockPin_id.id, _outputClockPin_id.activeHigh),
		_counterTimer(loopSubject),
		_displayCounter(_serialDataPin, _inputClockPin, _outputClockPin,
			_counterTimer, MAX, PERIOD_US/3)
	{}

	virtual ~ShiftedCounterBuilder() {}

	virtual Counter& get()
	{
		return _displayCounter;
	}

private:
	PinsDefinition<1> _serialDataPin_id;
	PinsDefinition<1> _inputClockPin_id;
	PinsDefinition<1> _outputClockPin_id;

	ArduinoPins<char,1> _serialDataPin;
	ArduinoPins<char,1> _inputClockPin;
	ArduinoPins<char,1> _outputClockPin;

	TimerImpl _counterTimer;
	ShiftedCounter _displayCounter;
};

#endif
