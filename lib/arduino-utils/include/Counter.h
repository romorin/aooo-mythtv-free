#ifndef COUNTER_H
#define COUNTER_H

#include "stddef.h"

class Counter
{
public:
	virtual ~Counter() {}

	virtual bool ready() const = 0;
	virtual const size_t currentCount() const = 0;
	virtual void increment() = 0;
	virtual size_t maxCount() const = 0;
};

#endif
