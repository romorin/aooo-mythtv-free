#ifndef DIRECT_COUNTER_H
#define DIRECT_COUNTER_H

#include "Counter.h"
#include "Pins.h"

template <typename HOLDER, int LENGTH>
class DirectCounter : public Counter
{
public:
	DirectCounter(Pins<HOLDER>& pins) :
		_pins(pins), _count(0)
	{
		_pins.write(1 << _count);
	}

	virtual ~DirectCounter() {}

	virtual bool ready() const
	{
		return true;
	}

	virtual const size_t currentCount() const
	{
		return _count;
	}

	virtual void increment()
	{
		if (_count + 1 >= LENGTH)
		{
			_count = 0;
		}
		else
		{
			++_count;
		}
		_pins.write(1 << _count);
	}

	virtual size_t maxCount() const
	{
		return LENGTH;
	}

private:
	Pins<HOLDER>& _pins;
	size_t _count;
};

#endif
