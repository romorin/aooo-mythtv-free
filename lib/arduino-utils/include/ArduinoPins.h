#ifndef ARDUINO_PINS_H
#define ARDUINO_PINS_H

#include "Pins.h"

#include <array>

#include "Arduino.h"
#include "stddef.h"

template <typename HOLDER, size_t LENGTH>
class ArduinoPins : public Pins<HOLDER> {
public:
	ArduinoPins(const std::array<char,LENGTH> pins, const bool activeHigh) :
		_pins(pins),
		_high_level(activeHigh ? HIGH : LOW),
		_low_level(activeHigh ? LOW : HIGH)
	{
		for (int i = 0; i < LENGTH; ++i)
		{
			pinMode(_pins[i], OUTPUT);
		}
	}

	virtual ~ArduinoPins() {}

	virtual void write(const HOLDER data)
	{
		for (int i = 0; i < LENGTH; ++i)
		{
			digitalWrite(_pins[i], data & (1 << i) ? _high_level : _low_level);
		}
	}

private:
	const std::array<char,LENGTH> _pins;

	const char _high_level;
	const char _low_level;
};

#endif
