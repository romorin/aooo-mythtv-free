#ifndef SERIAL_STARTER_H
#define SERIAL_STARTER_H

#include "DebugPrint.h"

class SerialStarter
{
public:
	SerialStarter()
	{
		DEBUG_BEGIN(115200);
		delay(3000);
		DEBUG_PRINTLN("START");
	}
};

#endif
