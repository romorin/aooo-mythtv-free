#ifndef SEVENSD_H
#define SEVENSD_H

#include "stddef.h"

class SevenSD {
public:
	virtual ~SevenSD() {}
	virtual void updateDisplay() = 0;
	virtual size_t availableDigits() const = 0;
};

#endif
