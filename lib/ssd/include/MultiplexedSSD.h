#ifndef MULTIPLEXED_SSD_H
#define MULTIPLEXED_SSD_H

#include "Counter.h"
#include "Pins.h"
#include "Timer.h"

#include "CharSelector.h"
#include "SevenSD.h"

#include "stddef.h"

class MultiplexedSSD : public SevenSD {
public:
	MultiplexedSSD(Pins<char>& segment_pins, Timer& timer,
		const unsigned long periodUs, Counter& counter,
		const CharSelector& selector);

	virtual ~MultiplexedSSD();

	virtual void updateDisplay();
	virtual size_t availableDigits() const;

private:
	Pins<char>& _segment_pins;
	Counter& _counter;
	const CharSelector& _selector;
};

#endif
