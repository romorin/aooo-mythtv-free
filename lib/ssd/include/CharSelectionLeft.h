#ifndef CHAR_SELECTION_LEFT_H
#define CHAR_SELECTION_LEFT_H

#include "CharSelectionStrategy.h"
#include "SsdContent.h"

#include "stddef.h"

class CharSelectionLeft : public CharSelectionStrategy
{
public:
	CharSelectionLeft();
	virtual ~CharSelectionLeft();

	virtual char getSegments(
		const size_t position,
		const size_t availableDigits,
		const SsdContent& content) const;
};

#endif
