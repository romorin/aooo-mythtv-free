#ifndef CHAR_SELECTION_RIGHT_H
#define CHAR_SELECTION_RIGHT_H

#include "CharSelectionStrategy.h"
#include "SsdContent.h"

#include "stddef.h"

class CharSelectionRight : public CharSelectionStrategy
{
public:
	CharSelectionRight();
	virtual ~CharSelectionRight();

	virtual char getSegments(
		const size_t position,
		const size_t availableDigits,
		const SsdContent& content) const;
};

#endif
