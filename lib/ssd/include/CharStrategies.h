#ifndef CHAR_STRATEGIES_H
#define CHAR_STRATEGIES_H

enum PositioningStrategy { LEFT, RIGHT, CENTER };
enum OverflowStrategy { BEGINNING, SCROLL, ERROR };

#endif
