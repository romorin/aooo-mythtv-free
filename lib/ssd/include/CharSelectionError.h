#ifndef CHAR_SELECTION_ERROR_H
#define CHAR_SELECTION_ERROR_H

#include "CharSelectionStrategy.h"
#include "SsdContent.h"

#include "stddef.h"

class CharSelectionError : public CharSelectionStrategy
{
public:
	CharSelectionError();
	virtual ~CharSelectionError();

	virtual char getSegments(
		const size_t position,
		const size_t availableDigits,
		const SsdContent& content) const;
};

#endif
