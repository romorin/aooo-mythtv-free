#include "CharSelectionError.h"

CharSelectionError::CharSelectionError()
{}

CharSelectionError::~CharSelectionError()
{}

char CharSelectionError::getSegments(
	const size_t, const size_t, const SsdContent& content) const
{
	return content.getSegments('E', false);
}
