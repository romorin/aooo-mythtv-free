#include "MultiplexedSSD.h"

MultiplexedSSD::MultiplexedSSD(Pins<char>& segment_pins, Timer& timer,
	const unsigned long periodUs, Counter& counter,
	const CharSelector& selector) :
		_segment_pins(segment_pins), _counter(counter),
		_selector(selector)
{
	timer.attach_us(periodUs, std::bind(&MultiplexedSSD::updateDisplay, this));
	_segment_pins.write(0);
}

MultiplexedSSD::~MultiplexedSSD() {}

void MultiplexedSSD::updateDisplay()
{
	if(!_counter.ready())
	{
		return;
	}

	_segment_pins.write(0);
	_counter.increment();
	_segment_pins.write(
		_selector.getSegments(_counter.currentCount(), _counter.maxCount())
	);
}

size_t MultiplexedSSD::availableDigits() const
{
	return _counter.maxCount();
}
