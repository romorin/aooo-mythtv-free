#include "CharliplexedSSD.h"

CharliplexedSSD::CharliplexedSSD(Pins<char>& segment_pins, Timer& timer,
	const unsigned long periodUs, Counter& counter,
	const CharSelector& selector) :
		_segment_pins(segment_pins), _counter(counter),
		_selector(selector), _activeSegment(0)
{
	timer.attach_us(periodUs, std::bind(&CharliplexedSSD::updateDisplay, this));
	_segment_pins.write(0);
}

CharliplexedSSD::~CharliplexedSSD() {}

void CharliplexedSSD::updateDisplay()
{
	if(!_counter.ready())
	{
		return;
	}

	_segment_pins.write(0);

	if (_activeSegment + 1 >= 7)
	{
		_counter.increment();
		_activeSegment = 0;
	}
	else
	{
		++_activeSegment;
	}
	_segment_pins.write(
		_selector.getSegments(_counter.currentCount(), _counter.maxCount())
		& (1 << _activeSegment));
}

size_t CharliplexedSSD::availableDigits() const
{
	return _counter.maxCount();
}
