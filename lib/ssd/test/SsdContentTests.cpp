#include "catch2/catch.hpp"

#include "SsdContentImpl.h"
#include "SegmentsDict.h"

#include "stubs/CharSelectionStub.h"

#include <string>

SCENARIO( "SsdContent works" ) {
	GIVEN( "A ssdContent and some stuff" ) {
		SegmentsDict dict;
		SsdContentImpl<3> content(dict);

		WHEN( "a text without dots is written" ) {
			content.writeString("HI");
			THEN ( "should return the segments" ) {
				REQUIRE( content.getSegments(0) == dict.getSegments('H', false));
				REQUIRE( content.getSegments(1) == dict.getSegments('I', false));
				REQUIRE( content.getSegments(2) == dict.getSegments(' ', false));
			}
		}
		AND_WHEN( "a text with dots is written" ) {
			content.writeString("HI.");
			THEN ( "should return the segments" ) {
				REQUIRE( (int)content.getSegments(0) == (int)dict.getSegments('H', false));
				REQUIRE( (int)content.getSegments(1) == (int)dict.getSegments('I', true));
				REQUIRE( (int)content.getSegments(2) == (int)dict.getSegments(' ', false));
			}
		}
		AND_WHEN( "a text with multiple dots is written" ) {
			content.writeString("....HI....");
			THEN ( "should return the segments" ) {
				REQUIRE( (int)content.getSegments(0) == (int)dict.getSegments('H', false));
				REQUIRE( (int)content.getSegments(1) == (int)dict.getSegments('I', true));
				REQUIRE( (int)content.getSegments(2) == (int)dict.getSegments(' ', false));
			}
		}
	}
}
