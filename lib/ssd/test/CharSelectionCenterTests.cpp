#include "catch2/catch.hpp"

#include "CharSelectionCenter.h"
#include "stubs/SsdContentStub.h"

#include <string>

#include "stddef.h"

SCENARIO( "CharSelectionCenter works" ) {
	GIVEN( "The strategy" ) {
		CharSelectionCenter selectionStrategy;
		SsdContentStub content;
		content.segment = 'z';
		content.len = 2;

		WHEN ( "A short text is used with 1 char of padding" ) {
			size_t availableDigits = 3;

			THEN ( "the caracters are correctly given" ) {
				REQUIRE( selectionStrategy.getSegments(0, availableDigits, content) == 'z' );
				REQUIRE( content.position == 0 );

				REQUIRE( selectionStrategy.getSegments(1, availableDigits, content) == 'z' );
				REQUIRE( content.position == 1 );
			}

			AND_THEN( "empty chars are given after the content" ) {
				REQUIRE( selectionStrategy.getSegments(2, availableDigits, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}

			AND_THEN( "empty chars are given out of bounds" ) {
				REQUIRE( selectionStrategy.getSegments(3, availableDigits, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}
		}
		WHEN ( "A short text is used with 2 char of padding" ) {
			size_t availableDigits = 4;

			THEN ( "the caracters are correctly given" ) {
				REQUIRE( selectionStrategy.getSegments(1, availableDigits, content) == 'z' );
				REQUIRE( content.position == 0 );

				REQUIRE( selectionStrategy.getSegments(2, availableDigits, content) == 'z' );
				REQUIRE( content.position == 1 );
			}

			AND_THEN( "empty chars are given before the content" ) {
				REQUIRE( selectionStrategy.getSegments(0, availableDigits, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );

				REQUIRE( selectionStrategy.getSegments(3, availableDigits, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}

			AND_THEN( "empty chars are given out of bounds" ) {
				REQUIRE( selectionStrategy.getSegments(4, availableDigits, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}
		}
	}
}
