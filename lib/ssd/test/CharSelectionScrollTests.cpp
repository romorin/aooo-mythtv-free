#include "catch2/catch.hpp"

#include "include/SubjectImpl.h"
#include "LoopEvent.h"
#include "CharSelectionScroll.h"

#include "stubs/SsdContentStub.h"

#include <string>

#include "stddef.h"

SCENARIO( "CharSelectionScroll works" ) {
	GIVEN( "The strategy" ) {
		unsigned long scrollMillis = 1;
		unsigned long edgePauseMillis = 2;
		SubjectImpl<LoopEvent> subject;

		CharSelectionScroll charSelectionStrategy(
			scrollMillis, edgePauseMillis, subject);

		SsdContentStub content;
		content.segment = 'z';

		WHEN ( "Scroll is not necessary" ) {
			content.len = 2;
			size_t availableDigits = 3;

			THEN ( "the caracters are correctly given" ) {
				REQUIRE(
					charSelectionStrategy.getSegments(0, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 0 );
				REQUIRE(
					charSelectionStrategy.getSegments(1, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 1 );
			}

			AND_THEN( "empty chars are given past the content" ) {
				REQUIRE(
					charSelectionStrategy.getSegments(2, availableDigits, content) == 'z'
				);
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}

			AND_THEN( "empty chars are given out of bounds" ) {
				REQUIRE(
					charSelectionStrategy.getSegments(3, availableDigits, content) == 'z'
				);
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}
		}

		AND_WHEN ( "Scroll is necessary" ) {
			content.len = 5;
			size_t availableDigits = 2;
			charSelectionStrategy.update({0, 0});

			THEN ( "the first set is given at the beginning" ) {
				REQUIRE(
					charSelectionStrategy.getSegments(0, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 0 );
				REQUIRE(
					charSelectionStrategy.getSegments(1, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 1 );
			}

			AND_THEN( "the second set is given after edgePauseMillis" ) {
				charSelectionStrategy.update(
					{edgePauseMillis, edgePauseMillis * 1000});
				REQUIRE(
					charSelectionStrategy.getSegments(0, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 1 );
				REQUIRE(
					charSelectionStrategy.getSegments(1, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 2 );
			}

			AND_THEN( "the thrid set is given after edgePauseMillis + scrollMillis" ) {
				unsigned int time = edgePauseMillis + scrollMillis;
				charSelectionStrategy.update({time , time * 1000});
				REQUIRE(
					charSelectionStrategy.getSegments(0, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 2 );
				REQUIRE(
					charSelectionStrategy.getSegments(1, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 3 );
			}

			AND_THEN( "the last set is given after edgePauseMillis + scrollMillis" ) {
				unsigned int time = edgePauseMillis + 2 * scrollMillis;
				charSelectionStrategy.update({time , time * 1000});
				REQUIRE(
					charSelectionStrategy.getSegments(0, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 3 );
				REQUIRE(
					charSelectionStrategy.getSegments(1, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 4 );
			}

			AND_THEN( "it gets back after a period" ) {
				unsigned int time = 2 * edgePauseMillis + 2 * scrollMillis;
				charSelectionStrategy.update({time , time * 1000});
				REQUIRE(
					charSelectionStrategy.getSegments(0, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 0 );
				REQUIRE(
					charSelectionStrategy.getSegments(1, availableDigits, content) == 'z'
				);
				REQUIRE( content.position == 1 );
			}
		}
	}
}
