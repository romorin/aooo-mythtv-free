#include "ButtonToHttpClient.h"

ButtonToHttpClient::ButtonToHttpClient(
	HttpClientShim& client,
	CharReceiver& receiver,
	const char* const host,
	const unsigned int port,
	const char* const uri
) : _client(client), _receiver(receiver), _host(host), _port(port), _uri(uri)
{
	_client.get(_receiver, _host, _port, _uri);
}

ButtonToHttpClient::~ButtonToHttpClient() {}

void ButtonToHttpClient::update(const bool& pressed)
{
	if (pressed)
	{
		_client.get(_receiver, _host, _port, _uri);
	}
}
