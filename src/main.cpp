#include <ESP8266WiFi.h>

// @todo wifi-client lib cannot find it if this include is removed
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include "Compositor.h"
#include "../password.h"

static const char* const HOST = "192.168.1.243";
static const unsigned int PORT = 6544;
static const char* const URI = "/Myth/GetStorageGroupDirs";

static const char* const PRE_PATTERN = "<KiBFree>";
static const char* const POST_PATTERN = "</KiBFree>";

// esp8266 does not like to have a simple object there (has delay) : rst cause:2
Compositor<>* compositor;

void setup()
{
	static Compositor<> comp(
		{{D0,D1,D2,D3,D4,D5,D6}, false},
		{{D7}, true},
		{{D8}, true},
		{{D9}, true},
		ssid, password, HOST, PORT, URI, PRE_PATTERN, POST_PATTERN
	);
	compositor = &comp;
}


void loop()
{
	compositor->loop();
}
