#include "SleepCounter.h"

#include "Arduino.h"

SleepCounter::SleepCounter(
	Subject<LoopEvent>& subject, const unsigned long timeUntilSleep) :
	_timeUntilSleep(timeUntilSleep), _started(false)
{
	subject.attach(*this);
}

SleepCounter::~SleepCounter() {}

void SleepCounter::update(const LoopEvent& event)
{
	if (!_started)
	{
		_started = true;
		_startTime = event.millis;
	}
	else
	{
		if (event.millis - _startTime > _timeUntilSleep)
		{
			ESP.deepSleep(0);
		}
	}
}
