#include "ResultToSsd.h"

#include <cstring>
#include "stddef.h"

ResultToSsd::ResultToSsd(SsdController& controller) : _controller(controller) {}

ResultToSsd::~ResultToSsd() {}

void ResultToSsd::onResult(const char* result)
{
	size_t toKeep = strlen(result) - 6;
	strncpy(_freeStr, result, toKeep);
	_freeStr[toKeep] = '\0';

	_controller.setPositioningStrategy(RIGHT);
	_controller.setOverflowStrategy(SCROLL);
	_controller.writeString(_freeStr);
}
