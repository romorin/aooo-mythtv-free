#ifndef RESULT_TO_SSD_H
#define RESULT_TO_SSD_H

#include "ResultHandler.h"
#include "SsdController.h"

class ResultToSsd : public ResultHandler
{
public:
	ResultToSsd(SsdController& controller);
	virtual ~ResultToSsd();

	virtual void onResult(const char* result);

private:
	SsdController& _controller;
	char _freeStr[8];
};

#endif
