#ifndef BUTTON_TO_HTTP_CLIENT_H
#define BUTTON_TO_HTTP_CLIENT_H

#include "include/Observer.h"
#include "HttpClientShim.h"
#include "CharReceiver.h"

class ButtonToHttpClient : public Observer<bool>
{
public:
	ButtonToHttpClient(
		HttpClientShim& client,
		CharReceiver& receiver,
		const char* const host,
		const unsigned int port,
		const char* const uri
	);

	virtual ~ButtonToHttpClient();
	virtual void update(const bool& pressed);

private:
	HttpClientShim& _client;
	CharReceiver& _receiver;
	const char* const _host;
	const unsigned int _port;
	const char* const _uri;
};

#endif
