#ifndef COMPOSITOR_H
#define COMPOSITOR_H

#include "include/SubjectImpl.h"

#include "SerialStarter.h"
#include "ShiftedCounterBuilder.h"
#include "LoopEvent.h"
#include "PinsDefinition.h"

#include "CharStrategies.h"
#include "SsdControllerBuilder.h"

#include "HttpClientShimImpl.h"
#include "PatternReceiver.h"
#include "BufferingReceiverImpl.h"
#include "FilteringCharReceiver.h"

#include "ResultToSsd.h"
#include "SleepCounter.h"

#include "Arduino.h"

template <
	unsigned long TIME_UNTIL_SLEEP = 10000,
	size_t NUM_SEGMENTS = 7,
	size_t NUM_DISPLAYS = 4,
	size_t MAX_CONTENT_LENGTH = 30,
	class TSSD = MultiplexedSSD,
	size_t PERIOD_US = 2000,
	size_t SCROLL_MILLIS = 500,
	size_t EDGE_SCROLL_MILLIS = 1000
>
class Compositor
{
public:
	Compositor(PinsDefinition<NUM_SEGMENTS> segmentPinsId,
		PinsDefinition<1> serialDataPinId,
		PinsDefinition<1> inputClockPinId,
		PinsDefinition<1> outputClockPinId,
		const char* const ssid,
		const char* const password,
		const char* const host,
		const unsigned int port,
		const char* const uri,
		const char* const prePattern,
		const char* const postPattern) :
		_serialStarter(),
		_loopSubject(),
		_counterBuilder(_loopSubject, serialDataPinId,
			inputClockPinId, outputClockPinId),
		_controllerBuilder(segmentPinsId, _loopSubject, _counterBuilder.get()),
		_resultHandler(_controllerBuilder.get()),
		_prePattern(prePattern),
		_postPattern(postPattern),
		_buffer(),
		_eventHandler(_resultHandler, _prePattern, _postPattern, _buffer),
		_shim(ssid, password, _loopSubject),
		_sleepCounter(_loopSubject, TIME_UNTIL_SLEEP)
	{
		_shim.get(_eventHandler, host, port, uri);
	}

	void loop()
	{
		static LoopEvent event;
		event.millis = millis();
		event.micros = micros();
		_loopSubject.notify(event);
	}

private:
	SerialStarter _serialStarter;
	SubjectImpl<LoopEvent> _loopSubject;
	ShiftedCounterBuilder <NUM_DISPLAYS, PERIOD_US> _counterBuilder;
	SsdControllerBuilder <TSSD, PERIOD_US, SCROLL_MILLIS, EDGE_SCROLL_MILLIS,
		NUM_SEGMENTS, NUM_DISPLAYS, MAX_CONTENT_LENGTH> _controllerBuilder;

	ResultToSsd _resultHandler;
	PatternReceiver _prePattern;
	PatternReceiver _postPattern;
	BufferingReceiverImpl<20> _buffer;

	FilteringCharReceiver _eventHandler;
	HttpClientShimImpl _shim;
	SleepCounter _sleepCounter;
};

#endif
