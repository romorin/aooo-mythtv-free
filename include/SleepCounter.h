#ifndef SLEEP_COUNTER_H
#define SLEEP_COUNTER_H

#include "include/Observer.h"
#include "LoopEvent.h"
#include "include/Subject.h"

class SleepCounter : public Observer<LoopEvent>
{
public:
	SleepCounter(Subject<LoopEvent>& subject, const unsigned long timeUntilSleep);
	virtual ~SleepCounter();

	virtual void update(const LoopEvent& event);

private:
	const unsigned long _timeUntilSleep;
	unsigned long _startTime;
	bool _started;
};

#endif
